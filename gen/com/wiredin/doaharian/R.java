/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.wiredin.doaharian;

public final class R {
    public static final class anim {
        public static final int fade_in=0x7f040000;
        public static final int fade_out=0x7f040001;
        public static final int slide_down=0x7f040002;
        public static final int slide_in_left=0x7f040003;
        public static final int slide_in_right=0x7f040004;
        public static final int slide_out_left=0x7f040005;
        public static final int slide_out_right=0x7f040006;
        public static final int slide_up=0x7f040007;
    }
    public static final class array {
        public static final int nav_drawer_icons=0x7f090001;
        /**  Nav Drawer Menu Items 
         */
        public static final int nav_drawer_items=0x7f090000;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int counter_text_bg=0x7f060004;
        public static final int counter_text_color=0x7f060005;
        public static final int doa_divider=0x7f060006;
        public static final int hijau_doa=0x7f060007;
        public static final int list_background=0x7f060001;
        public static final int list_background_pressed=0x7f060002;
        public static final int list_divider=0x7f060003;
        public static final int list_item_title=0x7f060000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f070000;
        public static final int activity_vertical_margin=0x7f070001;
    }
    public static final class drawable {
        public static final int akhlakbaik=0x7f020000;
        public static final int alamikesulitan=0x7f020001;
        public static final int bamer_thumb=0x7f020002;
        public static final int bgdoa=0x7f020003;
        public static final int button_facebook=0x7f020004;
        public static final int button_grey=0x7f020005;
        public static final int button_hijau=0x7f020006;
        public static final int button_twitter=0x7f020007;
        public static final int counter_bg=0x7f020008;
        public static final int dengarpetir=0x7f020009;
        public static final int dh_bg1=0x7f02000a;
        public static final int dh_bg2=0x7f02000b;
        public static final int dh_bg3=0x7f02000c;
        public static final int dh_bg4=0x7f02000d;
        public static final int doaharianshareimg=0x7f02000e;
        public static final int fadhil_thumb=0x7f02000f;
        public static final int fared_thumb=0x7f020010;
        public static final int fitnahdunia=0x7f020011;
        public static final int hadi_thumb=0x7f020012;
        public static final int header_harian=0x7f020013;
        public static final int hindarmimpi=0x7f020014;
        public static final int ibubapa=0x7f020015;
        public static final int ic_drawer=0x7f020016;
        public static final int ic_launcher=0x7f020017;
        public static final int iharian=0x7f020018;
        public static final int iinfo=0x7f020019;
        public static final int info_wired=0x7f02001a;
        public static final int isolat=0x7f02001b;
        public static final int kebaikandunia=0x7f02001c;
        public static final int kelompokjahil=0x7f02001d;
        public static final int keluarmasjid=0x7f02001e;
        public static final int keluarrumah=0x7f02001f;
        public static final int keluartandas=0x7f020020;
        public static final int ketikaberhias=0x7f020021;
        public static final int ketikabersedih=0x7f020022;
        public static final int ketikahujan=0x7f020023;
        public static final int ketikamimpi=0x7f020024;
        public static final int lihatkeindahan=0x7f020025;
        public static final int lihatpakaibaru=0x7f020026;
        public static final int list_bg=0x7f020027;
        public static final int list_item_bg_normal=0x7f020028;
        public static final int list_item_bg_pressed=0x7f020029;
        public static final int list_selector=0x7f02002a;
        public static final int masukmasjid=0x7f02002b;
        public static final int masukrumah=0x7f02002c;
        public static final int masuktandas=0x7f02002d;
        public static final int mginstagramphoto=0x7f02002e;
        public static final int naikkenderaan=0x7f02002f;
        public static final int nazrin_thumb=0x7f020030;
        public static final int pakaianbaru=0x7f020031;
        public static final int parallax_wired=0x7f020032;
        public static final int pergimasjid=0x7f020033;
        public static final int play_btn=0x7f020034;
        public static final int sebelummakan=0x7f020035;
        public static final int sebelumtidur=0x7f020036;
        public static final int selepasmakan=0x7f020037;
        public static final int selepastidur=0x7f020038;
        public static final int splash_screen_bg=0x7f020039;
        public static final int tanggalpakaian=0x7f02003a;
        public static final int terimaamalan=0x7f02003b;
        public static final int ziarahkubur=0x7f02003c;
    }
    public static final class id {
        public static final int action_play=0x7f0c0038;
        public static final int action_settings=0x7f0c0039;
        public static final int background_level=0x7f0c002c;
        public static final int background_level_flip1=0x7f0c002d;
        public static final int btnFacebook=0x7f0c0009;
        public static final int btnInstagram=0x7f0c0007;
        public static final int btnSumber1=0x7f0c000e;
        public static final int btnSumber2=0x7f0c0012;
        public static final int btnSumber3=0x7f0c0014;
        public static final int btnSumber4=0x7f0c0018;
        public static final int btnSumber5=0x7f0c001c;
        public static final int btnTwitter=0x7f0c0008;
        public static final int buttonFacebook=0x7f0c0003;
        public static final int buttonTwitter=0x7f0c0002;
        public static final int counter=0x7f0c002a;
        public static final int dataDoa=0x7f0c0001;
        public static final int doaText=0x7f0c0027;
        public static final int drawer_layout=0x7f0c0004;
        public static final int flipper_harian=0x7f0c002b;
        public static final int frame_container=0x7f0c0005;
        public static final int icon=0x7f0c0028;
        public static final int imageView1=0x7f0c000a;
        public static final int imageView2=0x7f0c000f;
        public static final int imageView3=0x7f0c0013;
        public static final int imageView4=0x7f0c0017;
        public static final int imageView5=0x7f0c001b;
        public static final int infoBtnHubung=0x7f0c0034;
        public static final int infoBtnKongsi=0x7f0c0031;
        public static final int infoBtnNilai=0x7f0c0033;
        public static final int infoBtnSumber=0x7f0c0035;
        public static final int infoText1=0x7f0c002f;
        public static final int infoText2=0x7f0c0030;
        public static final int infoText3=0x7f0c0032;
        public static final int listDoaHarian=0x7f0c002e;
        public static final int list_slidermenu=0x7f0c0006;
        public static final int scrollView1=0x7f0c0000;
        public static final int textView1=0x7f0c000b;
        public static final int textView10=0x7f0c001d;
        public static final int textView11=0x7f0c001e;
        public static final int textView12=0x7f0c001f;
        public static final int textView13=0x7f0c0020;
        public static final int textView14=0x7f0c0021;
        public static final int textView15=0x7f0c0022;
        public static final int textView16=0x7f0c0023;
        public static final int textView17=0x7f0c0024;
        public static final int textView18=0x7f0c0025;
        public static final int textView2=0x7f0c000c;
        public static final int textView3=0x7f0c000d;
        public static final int textView4=0x7f0c0010;
        public static final int textView5=0x7f0c0011;
        public static final int textView6=0x7f0c0015;
        public static final int textView7=0x7f0c0016;
        public static final int textView8=0x7f0c0019;
        public static final int textView9=0x7f0c001a;
        public static final int title=0x7f0c0029;
        public static final int txtLabel=0x7f0c0036;
        public static final int webView1=0x7f0c0037;
        public static final int wiredinwebsite=0x7f0c0026;
    }
    public static final class layout {
        public static final int activity_detail=0x7f030000;
        public static final int activity_info_social=0x7f030001;
        public static final int activity_main=0x7f030002;
        public static final int activity_share=0x7f030003;
        public static final int activity_splash=0x7f030004;
        public static final int activity_sumber=0x7f030005;
        public static final int doa_list_view=0x7f030006;
        public static final int drawer_list_item=0x7f030007;
        public static final int fragment_harian=0x7f030008;
        public static final int fragment_info=0x7f030009;
        public static final int fragment_pages=0x7f03000a;
        public static final int fragment_solat=0x7f03000b;
        public static final int web_view=0x7f03000c;
    }
    public static final class menu {
        public static final int detail=0x7f0b0000;
        public static final int doa_harian=0x7f0b0001;
        public static final int info_social=0x7f0b0002;
        public static final int share=0x7f0b0003;
        public static final int splash=0x7f0b0004;
        public static final int sumber=0x7f0b0005;
    }
    public static final class raw {
        public static final int akhlakbaik=0x7f050000;
        public static final int alamikesulitan=0x7f050001;
        public static final int dengarpetir=0x7f050002;
        public static final int fitnahdunia=0x7f050003;
        public static final int hindarmimpi=0x7f050004;
        public static final int ibubapa=0x7f050005;
        public static final int kebaikandunia=0x7f050006;
        public static final int kelompokjahil=0x7f050007;
        public static final int keluarmasjid=0x7f050008;
        public static final int keluarrumah=0x7f050009;
        public static final int keluartandas=0x7f05000a;
        public static final int ketikaberhias=0x7f05000b;
        public static final int ketikabersedih=0x7f05000c;
        public static final int ketikahujan=0x7f05000d;
        public static final int ketikamimpi=0x7f05000e;
        public static final int lihatkehindahan=0x7f05000f;
        public static final int lihatpakaibaru=0x7f050010;
        public static final int masukmasjid=0x7f050011;
        public static final int masukrumah=0x7f050012;
        public static final int masuktandas=0x7f050013;
        public static final int naikkenderaan=0x7f050014;
        public static final int pakaianbaru=0x7f050015;
        public static final int pergimasjid=0x7f050016;
        public static final int sebelummakan=0x7f050017;
        public static final int sebelumtidur=0x7f050018;
        public static final int selepasmakan=0x7f050019;
        public static final int selepastidur=0x7f05001a;
        public static final int tanggalpakaian=0x7f05001b;
        public static final int terimaamalan=0x7f05001c;
        public static final int ziarahkubur=0x7f05001d;
    }
    public static final class string {
        public static final int action_play=0x7f080002;
        public static final int action_settings=0x7f080001;
        public static final int app_name=0x7f080000;
        /**  Content Description 
         */
        public static final int desc_list_item_icon=0x7f080006;
        public static final int drawer_close=0x7f080005;
        public static final int drawer_open=0x7f080004;
        public static final int hello_world=0x7f080003;
        public static final int title_activity_detail=0x7f080008;
        public static final int title_activity_doa_harian=0x7f080007;
        public static final int title_activity_info_social=0x7f08000c;
        public static final int title_activity_share=0x7f08000a;
        public static final int title_activity_sumber=0x7f080009;
        public static final int title_activity_web=0x7f08000b;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f0a0000;
        /**  Application theme. 
         */
        public static final int AppTheme=0x7f0a0001;
        public static final int CustomDialogTheme=0x7f0a0005;
        public static final int MyActionBar=0x7f0a0002;
        public static final int MyTextAppearance=0x7f0a0003;
        public static final int NoTitleBar=0x7f0a0004;
    }
}
