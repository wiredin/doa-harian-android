package com.wiredin.doaharian;

import com.viewpagerindicator.IconPagerAdapter;


import com.wiredin.doaharian.PlaceDoaFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PlaceImageAdapter extends FragmentPagerAdapter implements
		IconPagerAdapter {

	private int[] Images = new int[] { R.drawable.niat_sehari, R.drawable.niat_sebulan,
			R.drawable.niat_berbuka

	};

	protected static final int[] ICONS = new int[] { R.drawable.circle,
			R.drawable.circle, R.drawable.circle };

	private int mCount = Images.length;

	public PlaceImageAdapter(Fragment fragment) {
		super(fragment.getChildFragmentManager());
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public int getIconResId(int index) {
		// TODO Auto-generated method stub
		return ICONS[index % ICONS.length];
	}

	@Override
	public Fragment getItem(int position) {
		// TODO Auto-generated method stub
		return new PlaceDoaFragment(Images[position]);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mCount;
	}

}
