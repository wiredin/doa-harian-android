package com.wiredin.doaharian;

import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

public class InfoFragment extends Fragment {
	
	PopupWindow popUp;
    LinearLayout layout;
    TextView tv;
    LayoutParams params;
    LinearLayout mainLayout;
	boolean click = true;
	
	public InfoFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		
		popUp = new PopupWindow(getActivity());
        layout = new LinearLayout(getActivity());
        mainLayout = new LinearLayout(getActivity());
        tv = new TextView(getActivity());
 
        View rootView = inflater.inflate(R.layout.fragment_info, container, false);
        
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ProximaNova-Semibold.otf");
        
        TextView infoText1 = (TextView) rootView.findViewById(R.id.infoText1);
        TextView infoText2 = (TextView) rootView.findViewById(R.id.infoText2);
        TextView infoText3 = (TextView) rootView.findViewById(R.id.infoText3);
        
        infoText1.setTypeface(font);
        infoText2.setTypeface(font);
        infoText3.setTypeface(font);
        
        Button btnKongsi = (Button) rootView.findViewById(R.id.infoBtnKongsi);
        Button btnNilai = (Button) rootView.findViewById(R.id.infoBtnNilai);
        Button btnHubung = (Button) rootView.findViewById(R.id.infoBtnHubung);
        Button btnSumber = (Button) rootView.findViewById(R.id.infoBtnSumber);
        
        btnKongsi.setTypeface(font);
        btnNilai.setTypeface(font);
        btnHubung.setTypeface(font);
        btnSumber.setTypeface(font);
        
        btnKongsi.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent i = new Intent(getActivity(),
						ShareActivity.class);
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
		});
        
        btnNilai.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
				try {
				    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
				    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
				}
            }
		});
        
        btnHubung.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("plain/text");
				intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "doa@wiredin.my" });
				intent.putExtra(Intent.EXTRA_SUBJECT, "Doa Harian");
				intent.putExtra(Intent.EXTRA_TEXT, "");
				startActivity(Intent.createChooser(intent, ""));
            }
		});
        
        btnSumber.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent i = new Intent(getActivity(),
						SumberActivity.class);
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
		});
        
        return rootView;
    }
}
