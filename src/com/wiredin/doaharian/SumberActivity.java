package com.wiredin.doaharian;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

public class SumberActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sumber);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Button wiredin = (Button) findViewById(R.id.wiredinwebsite);
		
		wiredin.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent i = new Intent(SumberActivity.this,
						WebViewActivity.class);
				startActivity(i);
				overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
		});
		
		Button fadhil = (Button) findViewById(R.id.btnSumber1);
		Button yeen = (Button) findViewById(R.id.btnSumber2);
		Button bamer = (Button) findViewById(R.id.btnSumber3);
		Button fared = (Button) findViewById(R.id.btnSumber4);
		Button hadi = (Button) findViewById(R.id.btnSumber5);
		
		fadhil.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent i = new Intent(SumberActivity.this,
						InfoSocial.class);
				i.putExtra("name", "Muhammad Fadhil");
				i.putExtra("facebook", "100000298973895");
				i.putExtra("twitter", "null");
				startActivity(i);
				overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
		});
		
		yeen.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent i = new Intent(SumberActivity.this,
						InfoSocial.class);
				i.putExtra("name", "Muhammad Nazrin");
				i.putExtra("facebook", "1780890608");
				i.putExtra("twitter", "yenkerhack");
				startActivity(i);
				overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
		});
		
		bamer.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent i = new Intent(SumberActivity.this,
						InfoSocial.class);
				i.putExtra("name", "Amir Mustaqim");
				i.putExtra("facebook", "100000756314793");
				i.putExtra("twitter", "null");
				startActivity(i);
				overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
		});
		
		fared.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent i = new Intent(SumberActivity.this,
						InfoSocial.class);
				i.putExtra("name", "Farid Ridwan");
				i.putExtra("facebook", "100000065632873");
				i.putExtra("twitter", "faredhadi");
				startActivity(i);
				overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
		});
		
		hadi.setOnClickListener(new OnClickListener(){
			@Override
            public void onClick(View v) {
				Intent i = new Intent(SumberActivity.this,
						InfoSocial.class);
				i.putExtra("name", "Abdul Hadi");
				i.putExtra("facebook", "1396498214");
				i.putExtra("twitter", "hadian90");
				startActivity(i);
				overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
            }
		});
		
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {   
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        return true;
    }
	
	@Override
	public void onBackPressed() {
	   finish();
	   overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}

}
