package com.wiredin.doaharian;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;

public class ShareActivity extends Activity {
	Bitmap image;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);

		Typeface font = Typeface.createFromAsset(this.getAssets(),
				"fonts/ProximaNova-Semibold.otf");
		image = BitmapFactory.decodeResource(getResources(),
				R.drawable.doaharianshareimg);

		Button instagram = (Button) findViewById(R.id.btnInstagram);
		instagram.setTypeface(font);
		Button twitter = (Button) findViewById(R.id.btnTwitter);
		twitter.setTypeface(font);
		Button facebook = (Button) findViewById(R.id.btnFacebook);
		facebook.setTypeface(font);

		instagram.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
				ClipData clip = ClipData.newPlainText("Doa Harian", "Download Doa Harian app now! #DoaHarianApp http://wiredin.my/doa");
				clipboard.setPrimaryClip(clip);
				
				Intent intent = getPackageManager().getLaunchIntentForPackage(
						"com.instagram.android");
				if (intent != null) {
					Intent shareIntent = new Intent();
					shareIntent.setAction(Intent.ACTION_SEND);
					shareIntent.setPackage("com.instagram.android");
					try {
						shareIntent.putExtra(
								android.content.Intent.EXTRA_STREAM,
								Uri.parse(MediaStore.Images.Media
										.insertImage(getContentResolver(),
												image, "Doa Harian",
												"Download Doa Harian app now! #DoaHarianApp http://wiredin.my/doa")));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Log.d("Hadian", "error - " + e);
					}
					shareIntent.setType("image/*");

					startActivity(shareIntent);
					Toast.makeText(ShareActivity.this, "Paste our clipboard content to your caption!!", Toast.LENGTH_LONG).show();
				} else {
					// bring user to the market to download the app.
					// or let them choose an app?
					intent = new Intent(Intent.ACTION_VIEW);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setData(Uri.parse("market://details?id="
							+ "com.instagram.android"));
					startActivity(intent);
				}
			}

		});

		twitter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = getPackageManager().getLaunchIntentForPackage(
						"com.twitter.android");
				if (intent != null) {
					Intent shareIntent = new Intent();
					shareIntent.setAction(Intent.ACTION_SEND);
					shareIntent.setPackage("com.twitter.android");
					try {

						shareIntent
								.putExtra(android.content.Intent.EXTRA_TEXT,
										"Download Doa Harian app now! #DoaHarianApp http://wiredin.my/doa");
						shareIntent.putExtra(
								android.content.Intent.EXTRA_STREAM,
								Uri.parse(MediaStore.Images.Media
										.insertImage(getContentResolver(),
												image, "Doa Harian",
												"Download Doa Harian app now! #DoaHarianApp http://wiredin.my/doa")));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Log.d("Hadian", "error - " + e);
					}
					shareIntent.setType("image/*");

					startActivity(shareIntent);
				} else {
					// bring user to the market to download the app.
					// or let them choose an app?
					intent = new Intent(Intent.ACTION_VIEW);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setData(Uri.parse("market://details?id="
							+ "com.twitter.android"));
					startActivity(intent);
				}
			}

		});

		facebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
				ClipData clip = ClipData.newPlainText("Doa Harian", "Download Doa Harian app now! #DoaHarianApp http://wiredin.my/doa");
				clipboard.setPrimaryClip(clip);

				Intent intent = getPackageManager().getLaunchIntentForPackage(
						"com.facebook.katana");
				if (intent != null) {
					Intent shareIntent = new Intent();
					shareIntent.setAction(Intent.ACTION_SEND);
					shareIntent.setPackage("com.facebook.katana");
					try {
						shareIntent.putExtra(
								android.content.Intent.EXTRA_STREAM,
								Uri.parse(MediaStore.Images.Media
										.insertImage(getContentResolver(),
												image, "Doa Harian",
												"Download Doa Harian app now! #DoaHarianApp http://wiredin.my/doa")));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Log.d("Hadian", "error - " + e);
					}
					shareIntent.setType("image/*");

					startActivity(shareIntent);
					Toast.makeText(ShareActivity.this, "Paste our clipboard content to your post!!", Toast.LENGTH_LONG).show();
				} else {
					// bring user to the market to download the app.
					// or let them choose an app?
					intent = new Intent(Intent.ACTION_VIEW);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setData(Uri.parse("market://details?id="
							+ "com.facebook.katana"));
					startActivity(intent);
				}
			}

		});
	}

}
