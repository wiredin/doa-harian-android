package com.wiredin.doaharian;

import com.joanzapata.pdfview.PDFView;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class ActivityPanduanTarawikh extends Activity {
	
	PDFView pdfView;
	ImageView imgTutor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_solat);
		
		this.getActionBar().setTitle("Panduan Tarawikh");
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		pdfView = (PDFView) findViewById(R.id.pdfView);
		pdfView.fromAsset("solat_terawih21.pdf").enableSwipe(true).load();
		
		imgTutor = (ImageView) findViewById(R.id.img_tutor);
		imgTutor.setVisibility(View.GONE);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		finish();
		overridePendingTransition(R.anim.slide_in_left,
				R.anim.slide_out_right);
		return true;
	}

}
