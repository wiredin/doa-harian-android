package com.wiredin.doaharian;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;

public class DetailActivity extends Activity {
	
	int doaName;
	String doaPicName = null;
	MediaPlayer mediaPlayer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			doaName = Integer.parseInt(extras.getString("doa"));
		}

		getActionBar().setDisplayHomeAsUpEnabled(true);

		switch (doaName) {
		case 1:
			doaPicName = "sebelumtidur";
			break;
		case 2:
			doaPicName = "selepastidur";
			break;
		case 3:
			doaPicName = "sebelummakan";
			break;
		case 4:
			doaPicName = "selepasmakan";
			break;
		case 5:
			doaPicName = "keluarrumah";
			break;
		case 6:
			doaPicName = "masukrumah";
			break;
		case 7:
			doaPicName = "pergimasjid";
			break;
		case 8:
			doaPicName = "masukmasjid";
			break;
		case 9:
			doaPicName = "keluarmasjid";
			break;
		case 10:
			doaPicName = "naikkenderaan";
			break;
		case 11:
			doaPicName = "pakaianbaru";
			break;
		case 12:
			doaPicName = "lihatpakaibaru";
			break;
		case 13:
			doaPicName = "tanggalpakaian";
			break;
		case 14:
			doaPicName = "alamikesulitan";
			break;
		case 15:
			doaPicName = "lihatkeindahan";
			break;
		case 16:
			doaPicName = "fitnahdunia";
			break;
		case 17:
			doaPicName = "kelompokjahil";
			break;
		case 18:
			doaPicName = "terimaamalan";
			break;
		case 19:
			doaPicName = "ketikamimpi";
			break;
		case 20:
			doaPicName = "hindarmimpi";
			break;
		case 21:
			doaPicName = "ketikahujan";
			break;
		case 22:
			doaPicName = "dengarpetir";
			break;
		case 23:
			doaPicName = "ibubapa";
			break;
		case 24:
			doaPicName = "ketikaberhias";
			break;
		case 25:
			doaPicName = "ziarahkubur";
			break;
		case 26:
			doaPicName = "akhlakbaik";
			break;
		case 27:
			doaPicName = "masuktandas";
			break;
		case 28:
			doaPicName = "keluartandas";
			break;
		case 29:
			doaPicName = "kebaikandunia";
			break;
		case 30:
			doaPicName = "ketikabersedih";
			break;
		}
		
		Log.d("Hadian", "nama doa: "+doaPicName);
		
		int doaid = getResources().getIdentifier("com.wiredin.doaharian:drawable/" + doaPicName, null, null);
		
		ImageView doaData = (ImageView) findViewById(R.id.dataDoa);
		doaData.setImageResource(doaid);
	}
	
	@Override
	public void onBackPressed() {
	   finish();
	   overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {   
		switch (menuItem.getItemId()) {
        case R.id.action_play:
        	playSound(doaPicName);
            return true;
        default:
        	finish();
    		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
		}
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.detail, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	private void playSound(String doaName){
		if (mediaPlayer!=null){
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
		}
		
		int doasoundid = getResources().getIdentifier("com.wiredin.doaharian:raw/" + doaName, null, null);
		mediaPlayer = MediaPlayer.create(this, doasoundid);
		mediaPlayer.start();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		if (mediaPlayer!=null){
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
			mediaPlayer.release();
			mediaPlayer = null;
		}
		super.onStop();
	}

}
