package com.wiredin.doaharian;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

@SuppressLint("ValidFragment")
public class PlaceDoaFragment extends Fragment {

	int imageResourceId;

	public PlaceDoaFragment() {
		// TODO Auto-generated constructor stub

	}

	public PlaceDoaFragment(int i) {
		// TODO Auto-generated constructor stub
		imageResourceId = i;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		ImageView image = new ImageView(getActivity());
		image.setImageResource(imageResourceId);

		LinearLayout layout = new LinearLayout(getActivity());

		layout.setGravity(Gravity.CENTER);
		layout.addView(image);

		return layout;
	}

}
