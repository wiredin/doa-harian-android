package com.wiredin.doaharian;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class InfoSocial extends Activity {
	String name;
	String facebook;
	String twitter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info_social);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			name = extras.getString("name");
			facebook = extras.getString("facebook");
			twitter = extras.getString("twitter");

		}

		Button btnFacebook = (Button) findViewById(R.id.buttonFacebook);
		Button btnTwitter = (Button) findViewById(R.id.buttonTwitter);

		setTitle(name);

		if (twitter.equals("null")) {
			btnTwitter.setVisibility(View.GONE);
		} else {
			btnTwitter.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					startActivity(getOpenTwitterIntent(InfoSocial.this, twitter));
				}

			});
		}

		btnFacebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(getOpenFacebookIntent(InfoSocial.this, facebook));
			}

		});

	}

	public static Intent getOpenFacebookIntent(Context context,
			String facebookID) {

		Log.d("Hadian", "facebook: " + facebookID);

		try {
			context.getPackageManager()
					.getPackageInfo("com.facebook.katana", 0);
			return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/"
					+ facebookID));
		} catch (Exception e) {
			return new Intent(Intent.ACTION_VIEW,
					Uri.parse("https://www.facebook.com/" + facebookID));
		}

	}

	public static Intent getOpenTwitterIntent(Context context, String twitterID) {

		Log.d("Hadian", "twitter: " + twitterID);

		try {
			return new Intent(Intent.ACTION_VIEW,
					Uri.parse("twitter://user?screen_name=" + twitterID));
		} catch (Exception e) {
			return new Intent(Intent.ACTION_VIEW,
					Uri.parse("https://twitter.com/#!/" + twitterID));
		}

	}

}
