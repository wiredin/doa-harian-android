package com.wiredin.doaharian;

import com.joanzapata.pdfview.PDFView;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class PilihanActivity extends Activity {
	PDFView pdfView;
	int doaName;
	String doaDataName = null;
	ImageView imgTutor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_solat);
		
		this.getActionBar().setTitle("Doa Pilihan");

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			doaName = Integer.parseInt(extras.getString("doa"));
		}

		getActionBar().setDisplayHomeAsUpEnabled(true);

		switch (doaName) {
		case 1:
			doaDataName = "img_doa1.pdf";
			break;
		case 2:
			doaDataName = "img_doa2.pdf";
			break;
		case 3:
			doaDataName = "img_doa3.pdf";
			break;
		case 4:
			doaDataName = "img_doa4.pdf";
			break;
		case 5:
			doaDataName = "img_doa5.pdf";
			break;
		case 6:
			doaDataName = "img_doa6.pdf";
			break;
		case 7:
			doaDataName = "img_doa7.pdf";
			break;
		case 8:
			doaDataName = "img_doa8.pdf";
			break;
		case 9:
			doaDataName = "img_doa9.pdf";
			break;

		}

		Log.d("Hadian", "nama doa: " + doaDataName);

		pdfView = (PDFView) findViewById(R.id.pdfView);
		pdfView.fromAsset(doaDataName).enableSwipe(false).load();

		imgTutor = (ImageView) findViewById(R.id.img_tutor);
		imgTutor.setVisibility(View.GONE);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
		case R.id.action_info:
			imgTutor.setVisibility(View.VISIBLE);
			new CountDownTimer(2000, 100) {

				public void onTick(long millisUntilFinished) {
					// implement whatever you want for every tick
				}

				public void onFinish() {
					imgTutor.setVisibility(View.GONE);
				}
			}.start();

			return true;
		default:
			finish();
			overridePendingTransition(R.anim.slide_in_left,
					R.anim.slide_out_right);
			return true;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.solat, menu);
		return super.onCreateOptionsMenu(menu);
	}

}
