package com.wiredin.doaharian;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class DoaSolatFragment extends Fragment {

	ViewFlipper viewFlipper;
	ListView listview;

	public DoaSolatFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_solat, container,
				false);

		viewFlipper = (ViewFlipper) rootView.findViewById(R.id.flipper_harian);
		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_in));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_out));
		viewFlipper.setFlipInterval(3000);
		viewFlipper.startFlipping();

		listview = (ListView) rootView.findViewById(R.id.listSolat);

		// add listview header
		ImageView imgv = new ImageView(getActivity());
		imgv.setImageResource(R.drawable.header_harian);
		imgv.setPadding(0, 95, 0, 75);
		listview.addHeaderView(imgv, null, false);

		String[] values = new String[] { "Solat Sunat Awwabin",
				"Solat Sunat Azan", "Solat Sunat Dhuha", "Solat Sunat Gerhana",
				"Solat Sunat Hajat", "Solat Sunat Hari Raya",
				"Solat Sunat Ihram", "Solat Sunat Istikharah",
				"Solat Sunat Ististiqah", "Solat Sunat Musafir",
				"Solat Sunat Mutlak", "Solat Sunat Nikah",
				"Solat Sunat Rawatib", "Solat Sunat Jumaat",
				"Solat Sunat Tahajjud", "Solat Sunat Tahiyatul Masjid",
				"Solat Sunat Tarawikh", "Solat Sunat Tasbih",
				"Solat Sunat Taubat", "Solat Sunat Tawaf", "Solat Sunat Witir",
				"Solat Sunat Wudhuk"};

		final StableArrayAdapter adapter = new StableArrayAdapter(
				getActivity(), R.layout.doa_list_view, values,
				"fonts/ProximaNova-Semibold.otf");
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					int position, long id) {
				// final String item = (String)
				// parent.getItemAtPosition(position);

				Intent i = new Intent(getActivity(), SolatActivity.class);
				i.putExtra("solat", String.valueOf(position));
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}

		});

		return rootView;
	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		// HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
		Typeface tf;
		Activity ctx;
		String[] objects;

		public StableArrayAdapter(Activity context, int textViewResourceId,
				String[] objects, String FONT) {
			super(context, textViewResourceId, objects);

			tf = Typeface.createFromAsset(context.getAssets(), FONT);
			ctx = context;
			this.objects = objects;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View rowView = convertView;

			if (rowView == null) {
				LayoutInflater inflater = ctx.getLayoutInflater();
				rowView = inflater.inflate(R.layout.doa_list_view, null);
			}

			TextView firstLine = (TextView) rowView.findViewById(R.id.doaText);
			firstLine.setText(objects[position]);
			firstLine.setTypeface(tf);

			return rowView;

		}
	}
}
