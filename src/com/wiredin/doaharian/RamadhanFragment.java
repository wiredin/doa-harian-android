package com.wiredin.doaharian;

import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import android.support.v4.app.Fragment;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

@SuppressLint("ValidFragment")
public class RamadhanFragment extends Fragment {

	public RamadhanFragment() {
	}

	PlaceImageAdapter mAdapter;
	ViewPager mPager;
	PageIndicator mIndicator;

	ImageView tarawikh, doa1, doa2, doa3, doa4, doa5, doa6, doa7, doa8, doa9;

	public static final String TAG = "ramadhanFragment";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_ramadhan, container,
				false);

		this.getActivity().getActionBar().setTitle("Ramadhan");

		mAdapter = new PlaceImageAdapter(this);

		mPager = (ViewPager) view.findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);

		mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);

		tarawikh = (ImageView) view.findViewById(R.id.tarawikh);
		tarawikh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(),
						ActivityPanduanTarawikh.class);
				startActivity(i);
			}
		});
		doa1 = (ImageView) view.findViewById(R.id.imageView1);
		doa1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "1");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		doa2 = (ImageView) view.findViewById(R.id.imageView2);
		doa2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "2");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		doa3 = (ImageView) view.findViewById(R.id.imageView3);
		doa3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "3");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		doa4 = (ImageView) view.findViewById(R.id.imageView4);
		doa4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "4");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		doa5 = (ImageView) view.findViewById(R.id.imageView5);
		doa5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "5");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		doa6 = (ImageView) view.findViewById(R.id.imageView6);
		doa6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "6");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		doa7 = (ImageView) view.findViewById(R.id.imageView7);
		doa7.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "7");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		doa8 = (ImageView) view.findViewById(R.id.imageView8);
		doa8.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "8");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		doa9 = (ImageView) view.findViewById(R.id.imageView9);
		doa9.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity(), PilihanActivity.class);
				i.putExtra("doa", "9");
				startActivity(i);
				getActivity().overridePendingTransition(R.anim.slide_in_right,
						R.anim.slide_out_left);
			}
		});
		return view;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mAdapter = null;
		mPager = null;
		mIndicator = null;
	}

}
