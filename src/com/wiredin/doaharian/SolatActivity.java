package com.wiredin.doaharian;

import com.joanzapata.pdfview.PDFView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class SolatActivity extends Activity {

	PDFView pdfView;
	int solatName;
	String solatDataName = null;
	ImageView imgTutor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_solat);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			solatName = Integer.parseInt(extras.getString("solat"));
		}

		getActionBar().setDisplayHomeAsUpEnabled(true);

		switch (solatName) {
		case 1:
			solatDataName = "awwabin.pdf";
			break;
		case 2:
			solatDataName = "azan.pdf";
			break;
		case 3:
			solatDataName = "dhuha.pdf";
			break;
		case 4:
			solatDataName = "gerhana.pdf";
			break;
		case 5:
			solatDataName = "hajat.pdf";
			break;
		case 6:
			solatDataName = "hariraya.pdf";
			break;
		case 7:
			solatDataName = "ihram.pdf";
			break;
		case 8:
			solatDataName = "istikharah.pdf";
			break;
		case 9:
			solatDataName = "ististiqa.pdf";
			break;
		case 10:
			solatDataName = "musafir.pdf";
			break;
		case 11:
			solatDataName = "mutlak.pdf";
			break;
		case 12:
			solatDataName = "nikah.pdf";
			break;
		case 13:
			solatDataName = "rawatib.pdf";
			break;
		case 14:
			solatDataName = "sunatjumaat.pdf";
			break;
		case 15:
			solatDataName = "tahajud.pdf";
			break;
		case 16:
			solatDataName = "tahiyatulmasjid.pdf";
			break;
		case 17:
			solatDataName = "tarawikh.pdf";
			break;
		case 18:
			solatDataName = "tasbih.pdf";
			break;
		case 19:
			solatDataName = "taubat.pdf";
			break;
		case 20:
			solatDataName = "tawaf.pdf";
			break;
		case 21:
			solatDataName = "witir.pdf";
			break;
		case 22:
			solatDataName = "wudhuk.pdf";
			break;
		}
		
		Log.d("Hadian", "nama doa: "+solatDataName);

		pdfView = (PDFView) findViewById(R.id.pdfView);
		pdfView.fromAsset(solatDataName).enableSwipe(false).load();
		
		imgTutor = (ImageView) findViewById(R.id.img_tutor);
		imgTutor.setVisibility(View.GONE);
	}
	
	

	@Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {   
		switch (menuItem.getItemId()) {
        case R.id.action_info:
        	imgTutor.setVisibility(View.VISIBLE);
        	new CountDownTimer(2000, 100) {

        	     public void onTick(long millisUntilFinished) {
        	        // implement whatever you want for every tick
        	     }

        	     public void onFinish() {
        	    	 imgTutor.setVisibility(View.GONE);
        	     }
        	  }.start();
        	
            return true;
        default:
        	finish();
    		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            return true;
		}
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.solat, menu);
	    return super.onCreateOptionsMenu(menu);
	}

}
